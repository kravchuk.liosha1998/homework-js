"use strict";

const buttons = document.querySelectorAll(".btn");

function BlueBtn(el) {
  el.classList.add("btn-color");
}

function BlackBtn() {
  for (let i = 0; i < buttons.length; i++) {
    buttons[i].classList.remove("btn-color");
  }
}

document.addEventListener("keydown", function (e) {
  BlackBtn();
  for (let i = 0; i < buttons.length; i++) {
    if (buttons[i].innerHTML.toUpperCase() === e.key.toUpperCase()) {
      BlueBtn(buttons[i]);
      return;
    }
  }
});
