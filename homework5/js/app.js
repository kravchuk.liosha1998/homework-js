"use strict";

function createNewUser() {
  let firstname = prompt("Enter your name", "Andrii");
  let lastname = prompt("Enter your surname", "Metelskyi");
  let birthday = prompt("Enter your birthday", "15.10.1994");

  let newUser = {
    firstname,
    lastname,
    birthday,

    getLogin() {
      return (this.firstname[0] + this.lastname).toLowerCase();
    },

    getAge() {
      let dayToday = new Date().getDate();
      let monthToday = new Date().getMonth() + 1;
      let yearToday = new Date().getFullYear();

      let userDay = birthday.slice(0, 2);
      let userMonth = birthday.slice(3, 5);
      let userYear = birthday.slice(6, 10);

      let userAge = yearToday - userYear;
      if (
        userMonth > monthToday ||
        (userMonth === monthToday && userDay > dayToday)
      ) {
        userAge--;
      }
      return userAge;
    },

    getPassword() {
      return (
        this.firstname[0].toUpperCase() +
        this.lastname.toLowerCase() +
        birthday.slice(6, 10)
      );
    },
  };

  return newUser;
}

let userForMax = createNewUser();

console.log(userForMax.getLogin()),
  console.log(userForMax.getPassword()),
  console.log(userForMax.getAge());
