"use strict";

const newArray = [
  "hello",
  "world",
  "Kiev",
  "Kharkiv",
  "Odessa",
  "Lviv",
  "1",
  "2",
  "3",
  "sea",
  "user",
  23,
];

const renderList = (arr, dbody = document.body) => {
  let li = arr.map((e) => `<li>${e}</li>`);
  let clearLi = li.join("");
  let ul = document.createElement("ul");
  ul.innerHTML = clearLi;
  dbody.prepend(ul);
};

renderList(newArray);
