const slides = document.querySelectorAll("#list .slide");
let currentSlide = 0;
const stop = document.getElementById("stop");
stop.addEventListener("click", pause);
const start = document.getElementById("start");
start.addEventListener("click", play);

let go = setInterval(carousel, 3000);
let showing = true;

function carousel() {
  slides[currentSlide].className = "slide";
  currentSlide = (currentSlide + 1) % slides.length;
  slides[currentSlide].className = "slide show";
}

function pause() {
  clearInterval(go);
  showing = false;
}

function play() {
  if (!showing) {
    go = setInterval(carousel, 3000);
    showing = true;
  }
}
